README for ICD10PCS Parser & Plugin
-----------------------------------

This is a python XML-to-JSON parser for the ICD 10 PCS codes and plugin for
AuShadha EMR

For license / redistribution of XML code and permissions please see their CDC website. 

The Parser itself is licensed under GNU-GPl Version 3. Please see LICENSE.txt

